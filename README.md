#Merix Studio Challange

Application searches through SWAPI and displays data based on search.

##Installation
`npm install`
`npm start`

## Things used.

axios,react promise - for api request
history, react-router, router-dom - for route handling
semantic-ui-css, semantic-ui-react - for looks and feels
redux - managing state of app
