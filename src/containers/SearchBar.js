import React, { Component } from "react";
import { Input, Button, Dropdown } from "semantic-ui-react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { bindActionCreators } from "redux";
import { fetchSwapi } from "../actions";
import { displayError, displaySuccess } from "../utils/userHandling";
class SearchBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      term: "",
      category: "people"
    };
    this.categories = [
      { key: "people", text: "People", value: "people" },
      { key: "planets", text: "Planets", value: "planets" },
      { key: "films", text: "Films", value: "films" },
      { key: "species", text: "Species", value: "species" },
      { key: "vehicles", text: "Vehicles", value: "vehicles" },
      { key: "starships", text: "Starships", value: "starships" }
    ];
    this.onInputChange = this.onInputChange.bind(this);
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onCategorySelection = this.onCategorySelection.bind(this);
  }
  onInputChange(event) {
    this.setState({ term: event.target.value });
  }
  onCategorySelection(event, data) {
    this.setState({ category: data.value });
  }
  onFormSubmit(event) {
    event.preventDefault();
    //Prevents empty search
    if (!this.state.term) return displayError("Search cannot be empty");

    document.getElementById("search-input").value = "";
    this.setState({ term: "" });
    this.props.fetchSwapi(this.state.term, this.state.category);
  }
  render() {
    return (
      <form onSubmit={this.onFormSubmit}>
        <Input
          action={
            <Dropdown
              button
              basic
              floating
              options={this.categories}
              defaultValue="people"
              onChange={this.onCategorySelection}
            />
          }
          icon="search"
          id="search-input"
          iconPosition="left"
          placeholder="Search..."
          onChange={this.onInputChange}
        />
        <Button basic type="submit">
          Search
        </Button>
        <Link to="/results" className="ui button results-button">
          Results
        </Link>
      </form>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({ fetchSwapi }, dispatch);
}

export default connect(
  null,
  mapDispatchToProps
)(SearchBar);
