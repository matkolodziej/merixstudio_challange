import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import ReduxPromise from "redux-promise";
import { Router, Route } from "react-router";
import createBrowsersHistory from "history/createBrowserHistory";
import { Container } from "semantic-ui-react";
import App from "./components/App";
import Results from "./components/Results";
import Details from "./components/Details";
import reducers from "./reducers";
import "semantic-ui-css/semantic.min.css";

const createStoreWithMiddleware = applyMiddleware(ReduxPromise)(createStore);
const history = createBrowsersHistory();

ReactDOM.render(
  <Provider store={createStoreWithMiddleware(reducers)}>
    <Router history={history}>
      <Container>
        <Route exact path="/" component={App} />
        <Route path="/results" component={Results} />
        <Route path="/details" component={Details} />
      </Container>
    </Router>
  </Provider>,

  document.querySelector("#root")
);
