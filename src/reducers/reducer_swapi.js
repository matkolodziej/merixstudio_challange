import { FETCH_SWAPI } from "../actions/";

export default function(state = [], action) {
  switch (action.type) {
    case FETCH_SWAPI:
      const data = {
        category: action.category,
        apiData: action.payload
      };
      return [data, ...state];
    default:
      return state;
  }
}
