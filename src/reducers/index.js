import { combineReducers } from "redux";
import SwapiReducer from "./reducer_swapi";
const rootReducer = combineReducers({
  swapiData: SwapiReducer
});

export default rootReducer;
