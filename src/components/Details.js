import React, { Component } from "react";
import { connect } from "react-redux";
import { Container, Table } from "semantic-ui-react";

class Details extends Component {
  displayData(data, name) {
    const template = {
      people: {
        birth: {
          text: "Birth year: ",
          value: data.apiData.birth_year
        },
        height: {
          text: "Height: ",
          value: data.apiData.height
        },
        mass: {
          text: "Mass: ",
          value: data.apiData.mass
        },
        gender: {
          text: "Gender: ",
          value: data.apiData.gender
        },
        hair_color: {
          text: "Hair color: ",
          value: data.apiData.hair_color
        },
        skin_color: {
          text: "Skin color: ",
          value: data.apiData.skin_color
        },
        eye_color: {
          text: "Eye color: ",
          value: data.apiData.eye_color
        }
      },
      planet: {
        rotation_period: {
          text: "Rotation period:",
          value: data.apiData.rotation_period
        },
        orbital_period: {
          text: "Orbital period:",
          value: data.apiData.orbital_period
        },
        climate: {
          text: "Climate:",
          value: data.apiData.climate
        },
        diameter: {
          text: "Diameter:",
          value: data.apiData.diameter
        },
        gravity: {
          text: "Gravity:",
          value: data.apiData.gravity
        },
        terrain: {
          text: "Terrain:",
          value: data.apiData.terrain
        },
        population: {
          text: "Population",
          value: data.apiData.population
        }
      },
      film: {
        director: {
          text: "Director:",
          value: data.apiData.director
        },
        episode_id: {
          text: "Episode:",
          value: data.apiData.episode_id
        },
        release_date: {
          text: "Release date:",
          value: data.apiData.release_date
        },
        producer: {
          text: "Producer:",
          value: data.apiData.producer
        },
        opening_crawl: {
          text: "Opening:",
          value: data.apiData.opening_crawl
        }
      },
      specie: {
        classification: {
          text: "Classification:",
          value: data.apiData.classification
        },
        designation: {
          text: "Designation:",
          value: data.apiData.designation
        },
        average_height: {
          text: "Average height:",
          value: data.apiData.average_height
        },
        skin_colors: {
          text: "Skin colors:",
          value: data.apiData.skin_colors
        },
        eye_colors: {
          text: "Eye colors:",
          value: data.apiData.eye_colors
        },
        average_lifespan: {
          text: "Average lifespan:",
          value: data.apiData.average_lifespan
        },
        language: {
          text: "Language:",
          value: data.apiData.language
        }
      },
      vehicle: {
        model: {
          text: "Model:",
          value: data.apiData.model
        },
        vehicle_class: {
          text: "Vehicle class:",
          value: data.apiData.vehicle_class
        },
        manufacturer: {
          text: "Manufacturer:",
          value: data.apiData.manufacturer
        },
        cost_in_credits: {
          text: "Cost in credits:",
          value: data.apiData.cost_in_credits
        },
        length: {
          text: "Length:",
          value: data.apiData.length
        },
        max_atmosphering_speed: {
          text: "Max atmosphering speed:",
          value: data.apiData.max_atmosphering_speed
        },
        crew: {
          text: "Crew:",
          value: data.apiData.crew
        },
        passengers: {
          text: "Passengers:",
          value: data.apiData.passengers
        },
        cargo_capacity: {
          text: "Cargo capacity:",
          value: data.apiData.cargo_capacity
        }
      },
      starship: {
        model: {
          text: "Model:",
          value: data.apiData.model
        },
        starship_class: {
          text: "Starhsip class:",
          value: data.apiData.starship_class
        },
        manufacturer: {
          text: "Manufacturer:",
          value: data.apiData.manufacturer
        },
        cost_in_credits: {
          text: "Cost in credits:",
          value: data.apiData.cost_in_credits
        },
        length: {
          text: "Length:",
          value: data.apiData.length
        },
        hyperdrive_rating: {
          text: "Hyperdrive:",
          value: data.apiData.hyperdrive_rating
        },
        crew: {
          text: "Crew:",
          value: data.apiData.crew
        },
        passengers: {
          text: "Passengers:",
          value: data.apiData.passengers
        },
        cargo_capacity: {
          text: "Cargo capacity:",
          value: data.apiData.cargo_capacity
        }
      }
    };
    let finalRows = [];

    switch (data.category) {
      case "people": {
        for (let key in template.people) {
          finalRows.push(
            <Table.Row key={template.people[key].text}>
              <Table.Cell>{template.people[key].text}</Table.Cell>
              <Table.Cell>{template.people[key].value}</Table.Cell>
            </Table.Row>
          );
        }
        break;
      }
      case "planets": {
        for (let key in template.planet) {
          finalRows.push(
            <Table.Row key={template.planet[key].text}>
              <Table.Cell>{template.planet[key].text}</Table.Cell>
              <Table.Cell>{template.planet[key].value}</Table.Cell>
            </Table.Row>
          );
        }
        break;
      }
      case "films": {
        for (let key in template.film) {
          finalRows.push(
            <Table.Row key={template.film[key].text}>
              <Table.Cell>{template.film[key].text}</Table.Cell>
              <Table.Cell>{template.film[key].value}</Table.Cell>
            </Table.Row>
          );
        }
        break;
      }
      case "species": {
        for (let key in template.specie) {
          finalRows.push(
            <Table.Row key={template.specie[key].text}>
              <Table.Cell>{template.specie[key].text}</Table.Cell>
              <Table.Cell>{template.specie[key].value}</Table.Cell>
            </Table.Row>
          );
        }
        break;
      }
      case "vehicles": {
        for (let key in template.vehicle) {
          finalRows.push(
            <Table.Row key={template.vehicle[key].text}>
              <Table.Cell>{template.vehicle[key].text}</Table.Cell>
              <Table.Cell>{template.vehicle[key].value}</Table.Cell>
            </Table.Row>
          );
        }
        break;
      }
      case "starships": {
        for (let key in template.starship) {
          finalRows.push(
            <Table.Row key={template.starship[key].text}>
              <Table.Cell>{template.starship[key].text}</Table.Cell>
              <Table.Cell>{template.starship[key].value}</Table.Cell>
            </Table.Row>
          );
        }
        break;
      }
    }
    return (
      <Container className="details-container">
        <Table celled padded className="app-table">
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>
                <h1>{name}</h1>
              </Table.HeaderCell>
              <Table.HeaderCell>
                <h1>
                  {data.category.charAt(0).toUpperCase() +
                    data.category.substr(1)}
                </h1>
              </Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>{finalRows}</Table.Body>
        </Table>
      </Container>
    );
  }
  render() {
    const { name } = this.props.location.state;
    const data = this.props.swapiData.find(starWarsObject => {
      if (starWarsObject.apiData.name === name) return starWarsObject.apiData;
    });

    return this.displayData(data, name);
  }
}
function mapStateToProps({ swapiData }) {
  return { swapiData };
}

export default connect(mapStateToProps)(Details);
