import React, { Component } from "react";
import SearchBar from "../containers/SearchBar";

class App extends Component {
  render() {
    return (
      <div className="app-container">
        <img
          src="http://pngimg.com/uploads/star_wars_logo/star_wars_logo_PNG34.png"
          width="300px"
          height="300px"
          alt="StarWarsIMG"
        />
        <div className="app-container__content">
          <h1>Search for anything from Star Wars!</h1>
          <p id="user-info" />
          <SearchBar />
        </div>
      </div>
    );
  }
}

export default App;
