import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Table, Container } from "semantic-ui-react";
import { connect } from "react-redux";
export class Results extends Component {
  renderSwapi(swData) {
    if (typeof swData.apiData === "undefined") {
      return;
    }
    const name = swData.apiData.name;
    const category = swData.category;
    return (
      <Table.Row key={name}>
        <Table.Cell>
          <Link to={{ pathname: "/details", state: { name } }}>{name}</Link>
        </Table.Cell>
        <Table.Cell>{category}</Table.Cell>
      </Table.Row>
    );
  }

  render() {
    return (
      <Container className="results-container">
        <Table celled padded className="app-table">
          <Table.Header>
            <Table.Row>
              <Table.HeaderCell>Name</Table.HeaderCell>
              <Table.HeaderCell>Category</Table.HeaderCell>
            </Table.Row>
          </Table.Header>
          <Table.Body>{this.props.swapiData.map(this.renderSwapi)}</Table.Body>
        </Table>
      </Container>
    );
  }
}
function mapStateToProps({ swapiData }) {
  return { swapiData };
}

export default connect(mapStateToProps)(Results);
