export function displayError(term) {
  const userInfo = document.getElementById("user-info");
  userInfo.innerText = term;
  userInfo.style.color = "red";
}

export function displaySuccess(term) {
  const userInfo = document.getElementById("user-info");
  userInfo.innerText = term;
  userInfo.style.color = "green";
}
