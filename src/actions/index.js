import axios from "axios";
import { displayError, displaySuccess } from "../utils/userHandling";
const ROOT_URL = "https://swapi.co/api/";

export const FETCH_SWAPI = "FETCH_SWAPI";

export const fetchSwapi = (term, category) => {
  if (!term || !category) return;

  const url = `${ROOT_URL}${category}/?search=${term}`;

  return axios
    .get(url)
    .then(res => {
      if (typeof res.data.results[0] === "undefined")
        return displayError("Force is not with you, search was not found.");
      else {
        displaySuccess("The force is with you, search was found.");
        return {
          type: FETCH_SWAPI,
          payload: res.data.results[0],
          category
        };
      }
    })
    .catch(error => {
      displayError("Error occured, please check your spelling.");
      if (error.response) {
        displayError("Error occured, data not obtained from server.");
      } else if (error.request) {
        console.log(error.request);
      } else {
        console.log("Error", error.message);
      }
    });
};
